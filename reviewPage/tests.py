from django.test import TestCase, Client
from reviewPage.models import *
from reviewPage.urls import *
from reviewPage.forms import *

class ReviewTest(TestCase):
    def create_model(self):
        return ReviewModel.objects.create(
            produknya = "test01",
            namaReview = "testnama01",
            isiReview = "testisi01",
        )
    
    def test_str_model(self):
        review = self.create_model()
        self.assertEqual(str(review), 'testnama01')

    def test_apakah_ada_url_yang_menampilkan_review(self):
        review = self.create_model()
        c = Client()
        response = c.get('/products/desc/test01/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_url_yang_menampilkan_addReview(self):
        review = self.create_model()
        c = Client()
        response = c.get('/products/desc/test01/add')
        self.assertEqual(response.status_code, 200)