from django import forms
from .models import ReviewModel
from crispy_forms.helper import FormHelper

class ReviewForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = False

    class Meta:
        model = ReviewModel
        fields = { 'namaReview' , 'isiReview'}
