from django.db import models

# Create your models here.

class ReviewModel(models.Model):
    produknya = models.CharField(max_length = 20)
    namaReview = models.CharField(max_length = 100)
    isiReview = models.TextField(max_length = 400)

    def __str__(self):
        return self.namaReview

