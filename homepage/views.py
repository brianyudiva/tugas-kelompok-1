from django.shortcuts import render, redirect
from .forms import PersonForm
from .models import Person

# Create your views here.
def index(request):
    return render(request, 'base.html')
def home(request):
    if request.method == "POST":
        form = PersonForm(request.POST)
        if form.is_valid():
            if Person.objects.filter(name = request.POST['name']).exists():
                arguments = {
                    'form' : PersonForm(),
                    'ada' : True,
                    'tidak_valid' : False,
                }
                return render(request, 'home.html', arguments)
            else:
                form.save()
                return redirect('products:hair')
        arguments = {
            'form' : PersonForm(),
            'ada' : False,
            'tidak_valid' : True,
        }
        return render(request, 'home.html', arguments)
    
    form = PersonForm()

    arguments = {
        'form': form,
        'ada' : False,
        'tidak_valid' : False,
    }

    return render(request, 'home.html', arguments)


def summary(request):
    person = Person.objects.all()
    arguments = {
        'person' : person,
    }
    return render(request, 'summary.html', arguments)
