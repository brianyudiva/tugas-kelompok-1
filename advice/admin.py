from django.contrib import admin
from .models import AdviceModel

# Register your models here.
class AdviceModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(AdviceModel, AdviceModelAdmin)