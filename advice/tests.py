from django.test import TestCase, Client

# Create your tests here.
class AdviceTest(TestCase):
    def test_apakah_url_benar_ke_advice(self):
        c = Client()
        response = c.get('/advice/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_heading(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('We need your<br>advice', content)
        self.assertIn('<h1', content)

    def test_apakah_ada_jumbotron(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<section class="jumbotron">', content)

    def test_apakah_ada_form_nama(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<input', content)
        self.assertIn('name="namaAdvice"', content)

    def test_apakah_ada_form_advice(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<input', content)
        self.assertIn('name="isiAdvice"', content)

    def test_apakah_ada_tombol_submit(self):
        c = Client()
        response = c.get('/advice/')
        content = response.content.decode('utf-8')
        self.assertIn('<input type="submit"', content)
        self.assertIn('Send your advice', content)