from django import forms

class AdviceForm(forms.Form):
    # Nama
    namaAdvice = forms.CharField(label='Nama', max_length=200)

    # Isi advice
    isiAdvice = forms.CharField(label='Advice', widget=forms.Textarea, max_length=2000)