from django.shortcuts import render
from .forms import AdviceForm
from .models import AdviceModel

# Create your views here.
def index_advice(request):
    advice_form = AdviceForm()
    argument = {
        'advice_form' : advice_form
    }
    return render(request, 'advice/index.html', argument)

def terima_advice(request):
    advice_form = AdviceForm(request.POST)
    isianNama = request.POST['namaAdvice']
    isianAdvice = request.POST['isiAdvice']

    temp = AdviceModel(
        namaAdvice = isianNama,
        isiAdvice = isianAdvice,
    )

    temp.save()

    argument = {
        'advice_form' : advice_form
    }

    return render(request, 'advice/index.html', argument)
    