from django.db import models

# Create your models here.
class AdviceModel(models.Model):
    namaAdvice = models.CharField(max_length=200)
    isiAdvice = models.CharField(max_length=2000)

    def __str__(self):
        return self.isiAdvice
